package functional;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class FilterAndSlicing {
	
	public static void main(String[] args) {
		 
		List<Integer> intList = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
		
		intList.stream().forEach(System.out::print);
		
		// Filtering example  -- filter (...)
		// Find the even numbers from intList
		List<Integer> evenIntList = intList.stream()
											.filter(d -> {return (d%2 == 0);})
											.collect(toList());
		
		System.out.print("\nEven Numbers are: ");
		evenIntList.stream().forEach(System.out::print);
		

		// Slicing example -- use limit(...) or skip(..)
		
		// limit(...)
		List<Integer> first2EvenInts = intList.stream()
											 	.filter(d -> d%2 == 0)
											 	.limit(2)
											 	.collect(toList());
		System.out.print("\nFirst 2 even Numbers are:");
		first2EvenInts.stream().forEach(System.out::print);
		
		// skip(...)
		List<Integer> exceptFirst2EvenInts = intList.stream()
														.filter(d -> d%2 == 0)
														.skip(2)
														.collect(toList());
		System.out.print("\nExcept the first 2 even Numbers are:");
		exceptFirst2EvenInts.stream().forEach(System.out::print);
		
	}

}

